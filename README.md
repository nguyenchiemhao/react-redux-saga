Redux

1. What&#39;s Redux?

Redux is a predictable state container for JavaScript apps.

It helps you write applications that behave consistently, run in different environments (client, server, and native), and are easy to test. On top of that, it provides a great developer experience, such as live code editing combined with a time traveling debugger.

Above paragraph is official definition on Redux documents. In my opinions, Redux is a library that help developer manege state of application (React app). It reduces passing data between components by the way that using the data flow like this

![](https://lh6.googleusercontent.com/QBjFoGgB1LQcQ9tPEpSHAy_5vCGWpx39EE7O6oNK7eZEZ2oRTafV0S5CIcyeXF_VrlpSPHnT2YQV6R6iH2hf_3BEEDk-PL-81clx3AmT)

To better understand then we should know some main part of redux:

- Actions: Actions are payloads of information that send data from your application to your store. They are the _only_ source of information for the store.

- Reducer: Reducers specify how the application&#39;s state changes in response to actions sent to the store. Remember that actions only describe _ **what happened** _, but don&#39;t describe how the application&#39;s state changes.

- Store: The Store is the object that brings all state together. It&#39;s a unique data source.

- Data flow:
  - Redux architecture revolves around a strict unidirectional data flow.
  - This means that all data in an application follows the same lifecycle pattern, making the logic of your app more predictable and easier to understand. It also encourages data normalization, so that you don&#39;t end up with multiple, independent copies of the same data that are unaware of one another.

If you&#39;re still confuse let take a look at flowing picture, It&#39;s can help you understand a bit.

![](https://lh5.googleusercontent.com/9fTyAlZmOHQxZlUIe6PR04nSclrzTY87L9W2MNz-StyaAVTR264EdeIHMltKv6vqSfOtuMhGWU47725Tpsn7XgMwE4cXck8nJRkE1lHg)

1. When an event (event) will generate an action describing what is happening.
2. The action will perform the Reducer coordination of the event handling.
3. The Reducer relies on Action descriptions to know what changes need to be made on the State and make updates.
4. When the State is updated, of course. Triggers that are monitoring that state will receive an update.

(It&#39;s so hard to understand, right? Don&#39;t worry, I&#39;m certainly you will understand when you practise it.)

2. Why do we you Redux?

- Most libs like React, Angular, etc are built in such a way that components come to internal management of their states without any external libraries or tools. It will work well for applications with few components, but as the application gets larger, the management of states shared through components will turn into odd jobs.
- Currently Redux has over 53k starts on github.

3. When do we use Redux?

- There are many components nested and share the same State.
- Data consistency between components.

4. **How do we integrate Redux into a React application?**

In this case, I use React js.

1. Create a react app

npx create-react-app app-name

![](https://lh5.googleusercontent.com/kEu5KXjyjVA48NEr4scX2jeIiy7wxP6QahIRTVv9c8wS_1FNBWzagT7OCBAnBBfKfbYTGU7eLbqbPblUreeXwZfZcg4dnwlqJ8ZYBSY)

2. Install redux, react-redux, redux-saga

- use yarn:

yarn add redux react-redux redux-saga

- use npm:

npm install --save redux react-redux redux-saga

![](https://lh5.googleusercontent.com/u4z767HSikCISavAIQhp70MSJHM-m9aY_8WAg2RDT4_ShmR3CELoVdEdRmAuHY9n4xNZcdYLfkHiTttWQzzLE4rGJfhRoKJsH0b-DptY)

3. Configure redux (redux-saga)

- Create action type: Action type is a object that contain type of actions

![](https://lh4.googleusercontent.com/iDVErj2aFYp5gu7trl1yl8Cm_Dyi2Zy6EM5WpZ7kK973XzwJQXMNldfnc4rC_GihUjnAaM4BZx0kbiO0TffvMtzVv3d5iD7b0VL_npo0)

- Create actions: Action is the place to define steps for reducer. We can handle side-effects here

![](https://lh5.googleusercontent.com/S_QmSxiw1vLKhO_w6hB4q2srmuzB7WHGbIohRbgsYFfJk33iPVzc5yOB_CYZZxgdwiVOsgjJ-M7UlnBcIOYoQgnfEvJ0fVu6rHUw5SST)

- Create reducer: When a action is dispatched, Reducer will executes actions and updates state(if there are).

![](https://lh3.googleusercontent.com/SWp2J2IdUu3TNOSlUW72XCkjdQyL_fO65qox23r-fkUKiNgXqxY0XKZqbHEV6h_5elCVoHLKbIHiBEkuxiH6W3295TscE74HnPbGTFL-)

- Create root reducer:

![](https://lh4.googleusercontent.com/nnFQ2aLOlho-Id5Kcxwyq2Xmh_puvzGH5njlo0a6CkMKqDVp9zmrSlfFp--12xvLKaAPV3pueFtrm43FQY2gbdL_uon_kh2zZOGSWQlR)

- Create root saga:

![](https://lh3.googleusercontent.com/xtbjL8pR-PG6B07Mm9Isu0RH2QVpPd17yOMP6l8-l-qw43L9f0YO82DK56s54BS0lPLQfv0wLzGRwAeTU4eSDTeUyIPwuLBnBrSYnC2U)

- Configure redux:

![](https://lh5.googleusercontent.com/0HcEnqmMVDkEWDhJ7cAIF37TM71xfp8CjsYdL1OdXbOCDiPDX9SLHmM0dTSdluDiW9KyOSRvcxKJQQpZtkuQNpoMXjNdH5nHJQYvQFCH)

- Apply redux into App

![](https://lh4.googleusercontent.com/1vK4H9F8QfvEE9ZrEQ6Ju931vNpHHyswvUiKgtrABEbVzitD9zheb1GbCy2Y_Npd-lJImJ23Gx4e55W-JkHxlKf4YEIhOGw_ydIKm6wz)

- Connect and dispatch a action:

![](https://lh3.googleusercontent.com/Xnl2__3PZFXsz1Pa4iZvhH-CZa3zPlZlsfW-2U-HHDljrzonqUYsTnbghZAtrpUEhynPsAxcYjxluJwVeqfKWQxJyQVSZWYSTpY-VpjK)

- Success:

![](https://lh5.googleusercontent.com/GPiUPEwbvCRT_zCP06kM1jDsvMMefpBrCw20CcXe-QN3n0sv1qo1EdmQzt6kFYtOjikwWge6xBvKK2b57uixaFtw1mb_UYfzS-VeBVo)

5. Redux Saga:

**To work with Redux Saga, you need to know the following:**

- **The function\*** declaration (function keyword followed by an asterisk) defines a generator function, which returns a Generator object.

![](https://lh3.googleusercontent.com/zJGQj0UF-EEo4vVViFInIy_DP4gozlL-hSuMVdivO_xqupR41gKKYHP1IrBOeBUdF06cYyrbDgbP_uKpJ8EK9wSRjb6dValZJ4hc7thK)

![](https://lh6.googleusercontent.com/ZixFt9Arhjrqfpswzk-C3XYWVyWIWIBXU9DGo888yEnoqXvTgnOduEOYseoWgY9sM0V02ciCFyayDKeoBk4oUTB9QX-AscfQJf11Pnxw)

![](https://lh4.googleusercontent.com/lbvoUVjUCTEDE3hWoCL4UjuXAW0bsFnFXio0z13VTecDkpU7oCp13vf1xTAu_GbtMGL1hGaIFXe2oBTqdWYzrc0ZdZMkmptFziSkjSsK)

- **Blocking** and synchronous mean the same thing: you call the API, it hangs up the thread until it has some kind of answer and returns it to you.

- **Non-blocking** means that if an answer can&#39;t be returned rapidly, the API returns immediately with an error and does nothing else.

- fork() : (non-blocking function)
  - **fork(fn, ...args)**
  - Creates an Effect description that instructs the middleware to perform a _non-blocking call_ on fn
- take(): (blocking function)
  - **take(pattern)**
  - Creates an Effect description that instructs the middleware to wait for a specified action on the Store. The Generator is suspended until an action that matches pattern is dispatched.
- call(): (blocking function)
  - **call(fn, ...args)**
  - Creates an Effect description that instructs the middleware to call the function fn with args as arguments.
  - Usually use call() to fetch data
- put(): (non-blocking function)
  - **put(action)**
  - Creates an Effect description that instructs the middleware to schedule the dispatching of an action to the store.

**Example**

![](https://lh3.googleusercontent.com/45MvSjqBTjcGDwK6NloBJ0cH2I8ZvKV4OjSXUdVfdxUTQeG-c9RxbjHkfKelmNS4i-c_W0qUWI8yaM_kEmLSVmUGD0cSBqza4CEkg5Ik)

- Finish:

![](https://lh5.googleusercontent.com/VZekFECN6olOHKSgbrdw18zgBap_HAojmUFmzsq1vFU5aphMcCAKIbALCtLzTKQBAnbg_UJ-QrOBo_vv2YIYxK6v4ehmAgx_97-PQIEY)

![](https://lh6.googleusercontent.com/_AkE1LiLBnOeMVab5UpvmoDdtPBXJbvZmb-DsSFueow45QpIEecxR-F3f6-SJNfCd4RZ0BlYnMrFyVW8c-D9fwHDBVl_oNvb1zF_alM)

6. Principles in Redux

- The store is always the correct and reliable data source.
- The state is only readable, the only way to change the state is to create an action and let the reducer change the state.
- The Reducer function must be a Pure function (with the same input only for 1 single output).