const { actionsType } = require("./action");

const INITIAL_STATE = {
  todoList: [],
};

const todoReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case actionsType.GET_TODO:
      return state; // cap nhat state
    case actionsType.GET_TODO_SUCCESS:
      return { ...state, todoList: action.payload };
    default:
      return state;
  }
};

export default todoReducer;
