import { take, call, put } from "redux-saga/effects";
import { actionsType, handleSuccess } from "./action";
import Axios from "axios";

export function* listGetTodo() {
  while (true) {
    yield take(actionsType.GET_TODO); //action type
    const { data, status } = yield call(callApi);
    if (status === 200) {
      /// set state
      // goi 1 action
      yield put(handleSuccess(data));
    } else {
      // handle error
    }
  }
}

const callApi = async () => {
  return await Axios.get("https://jsonplaceholder.typicode.com/todos");
};
