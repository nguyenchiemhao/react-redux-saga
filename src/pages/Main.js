import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { fetchData, actionsType } from "./action";

function Main() {
  const dispatch = useDispatch();
  const todo = useSelector((state) => state.todo);
  console.log("todo ", todo);
  return (
    <button
      onClick={() =>dispatch({ type: actionsType.GET_TODO, action: fetchData })
        
      }
    >
      Howie
    </button>
  );
}

export default Main;
