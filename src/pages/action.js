export const actionsType = {
  GET_TODO: "GET_TODO",
  GET_TODO_SUCCESS: "GET_TODO_SUCCESS",
  GET_TODO_FAILURE: "GET_TODO_FAILURE",
};

//get data
export const fetchData = () => ({
  type: actionsType.GET_TODO,
});

//set data
export const handleSuccess = (payload) => ({
  type: actionsType.GET_TODO_SUCCESS,
  payload: payload, // chuyen cho reducer
});


// action = { type: "", action: func }