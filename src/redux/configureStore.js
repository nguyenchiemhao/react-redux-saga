import createSagaMiddleware from "redux-saga";
import { createStore, applyMiddleware } from "redux";
import rootReducer from "./reducer/rootReducer"; // entry point of reducer
import rootSaga from "./sagas/rootSaga"; // entry point of saga

const sagaMiddleware = createSagaMiddleware(); // create middleware

const configureStore = () => {
  // create and apply saga middleware
  const store = createStore(rootReducer, applyMiddleware(sagaMiddleware));
  sagaMiddleware.run(rootSaga); // run saga listener
  return store;
};

export default configureStore;
