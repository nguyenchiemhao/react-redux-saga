import { combineReducers } from "redux";
import taskReducer from "./task/taskReducer";
import todoReducer from "../../pages/reducer";

const rootReducer = combineReducers({
  task: taskReducer,
  todo: todoReducer,
});

export default rootReducer;
