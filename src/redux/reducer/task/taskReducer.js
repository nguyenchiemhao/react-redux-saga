import taskActionTypes from "../../actions/task/taskActionsType";

const INITIAL_STATE = {
  // initial state
  listTasks: [],
  err: {},
};
const taskReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case taskActionTypes.FETCH_TASK_SUCCESS:
      //return new state
      return { ...state, listTasks: action.payload };

    case taskActionTypes.FETCH_TASK_FAILED:
      // handle fetch task failed
      return { ...state, err: action.payload };

    default:
      return state;
  }
};

export default taskReducer;
