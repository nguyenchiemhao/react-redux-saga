const taskActionTypes = {
  FETCH_TASK: "FETCH_TASK",
  FETCH_TASK_SUCCESS: "FETCH_TASK_SUCCESS",
  FETCH_TASK_FAILED: "FETCH_TASK_FAILED",
};

export default taskActionTypes;
