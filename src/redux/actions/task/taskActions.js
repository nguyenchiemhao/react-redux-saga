import taskActionsType from "./taskActionsType";

export const fetchTask = () => ({
  type: taskActionsType.FETCH_TASK, // action types
});
export const fetchTaskSuccess = (data) => ({
  type: taskActionsType.FETCH_TASK_SUCCESS, // action types
  payload: data, //create new state
});
export const fetchTaskFailed = (err) => ({
  type: taskActionsType.FETCH_TASK_FAILED, // action types
  payload: err, //create new state
});

// import axios from "axios";

// export const fetchTask = () => {
//   return (dispatch) => {
//     axios
//       .get("https://jsonplaceholder.typicode.com/todos")
//       .then((res) => {
//         console.log(res.data);
//         dispatch(fetchTaskSuccess(res.data));
//       })
//       .catch((err) => console.log(err));
//   };
// };
