import { fork, take, call, put } from "redux-saga/effects";
import taskActionTypes from "../actions/task/taskActionsType";
import * as taskServices from "../../services/taskService";
import * as taskActions from "../../redux/actions/task/taskActions";
import { listGetTodo } from "../../pages/saga";

function* rootSaga() {
  console.log("root saga ne");
  yield fork(listGetTodo); // open new process to listen fetch task
}

export default rootSaga;
