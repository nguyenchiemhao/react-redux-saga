import axios from "axios";

export const getTasks = async () => {
  return axios
    .get("https://jsonplaceholder.typicode.com/todos")
    .then((res) => {
      return res;
    })
    .catch((err) => {
      console.log("err", err);
    });
};
