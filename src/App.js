import React from "react";
import "./App.css";
import { Provider } from "react-redux";
import configureStore from "./redux/configureStore";
import Main from "./pages/Main";

function App() {
  const storeConfiguration = configureStore(); // configure store
  return (
    //use Provider(provided by react-redux) component to apply store into app
    <Provider store={storeConfiguration}>
      <Main />
    </Provider>
  );
}

export default App;
